﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEditor;

[CustomEditor(typeof(Enemy))]
public class EnemyEditor : Editor 
{
	public static Object prefab = Resources.Load("Node");

	private static bool expanded;

	private bool add;

	public override void OnInspectorGUI()
	{
		#if UNITY_EDITOR
		DrawDefaultInspector();
		
		Enemy myScript = (Enemy)target;

		List<Node> nodes = myScript.getNodes();

		expanded = EditorGUILayout.Foldout(expanded, "Nodes");

		if(expanded)
		{
			int i = 1;
			foreach(Node n in nodes)
			{
				if(n != null)
				{

					GUILayout.BeginHorizontal();
					GUILayout.Space(15);

					GUILayout.Label("Node " + i);
					if(GUILayout.Button("Select"))
					{
						Selection.activeGameObject = n.gameObject;
					}
					if(GUILayout.Button("Remove"))
					{
						myScript.removeNode(n);
						DestroyImmediate(n.gameObject);
						break;
					}
					GUILayout.EndHorizontal();
				}
				else
				{
					myScript.removeNode(n);
					//DestroyImmediate(n.gameObject);
					break;
				}
				i++;
			}
		}
		if(!add)
		{
			if(GUILayout.Button("Add path node"))
			{
				add = true;
				//GameObject go = (GameObject)PrefabUtility.InstantiatePrefab(prefab);
				//Node n = go.GetComponent<Node>();
				//myScript.addNode(n);
				//myScript.BuildObject();
			}
		}
		else
		{
			GUI.enabled = false;
			GUILayout.Button("Add path node");
			GUI.enabled = true;
		}
		#endif
	}
	void OnGUI()
	{
		// show the "Link" cursor when the mouse is howering over this rectangle.

	}
	void OnSceneGUI()
	{
		#if UNITY_EDITOR
		if(add)
		{
			//EditorGUIUtility.AddCursorRect (new Rect(0, 0, Screen.width, Screen.height), MouseCursor.Link);	
			int c = GUIUtility.GetControlID(FocusType.Passive);
			Debug.Log(Event.current.type);
			if(Event.current.type == EventType.MouseDown)
			{
				GUIUtility.hotControl = c;
			}
			else if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
			{

				Ray worldRay = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
				RaycastHit hit;
				Vector3 nodePos;

				if (Physics.Raycast (worldRay, out hit))
				{
					nodePos = hit.point;

					Enemy myScript = (Enemy)target;
					Debug.Log("added");
					GameObject go = (GameObject)PrefabUtility.InstantiatePrefab(prefab);
					Node n = go.GetComponent<Node>();
					n.transform.position = nodePos;
					myScript.addNode(n);
					EditorUtility.SetDirty(myScript);
				}
				add = false;
				GUIUtility.hotControl = 0;
				Event.current.Use();
				Repaint();

			}
		}
		#endif
	}
}	
#endif