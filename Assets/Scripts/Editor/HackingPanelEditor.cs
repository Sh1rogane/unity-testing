﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;

using UnityEditor;

[CustomEditor(typeof(HackingPanel))]
public class HackingPanelEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		HackingPanel hp = target as HackingPanel;

		if(hp.hexagon != null)
		{
			GUI.enabled = false;
		}

		if(GUILayout.Button("Add hexagon here"))
		{
			GameObject go = (GameObject)PrefabUtility.InstantiatePrefab(Resources.Load("Hexagon",typeof(GameObject)));
			go.transform.SetParent(hp.transform.parent);
			go.transform.localScale = new Vector3(1,1,1);
			go.transform.localPosition = hp.transform.localPosition;

			hp.hexagon = go.GetComponent<Hexagon>();

			//hp.GetComponentInParent<HackingGame>().hexagons.Add(hp.hexagon);
		}
		GUI.enabled = true;
	}
}
#endif