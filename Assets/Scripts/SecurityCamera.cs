﻿using UnityEngine;
using System.Collections;

public class SecurityCamera : MonoBehaviour 
{
	private GameObject vision;
	private bool seePlayer;
	// Use this for initialization
	void Start () 
	{
		vision = transform.Find("Body/Lens/Vision/Cube").gameObject;
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag.Equals("Player"))
		{
			seePlayer = true;
			vision.renderer.material.color = new Color(0, 1, 0, 0.15f);
		}
	}
	void OnTriggerStay(Collider col)
	{
		if(col.gameObject.tag.Equals("Player"))
		{
			seePlayer = true;
			vision.renderer.material.color = new Color(0, 1, 0, 0.15f);
		}
	}
	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag.Equals("Player"))
		{
			seePlayer = false;
			vision.renderer.material.color = new Color(1, 0, 0, 0.15f);
		}
	}
}
