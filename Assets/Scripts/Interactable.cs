﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Interactable : MonoBehaviour {

	private RectTransform ui;
	private InteractUI intUI;
	private bool show;
	public string useText = "Use";


	public GameObject triggerObject;

	// Use this for initialization
	void Start () {
		ui = GameObject.Find("InteractUI").GetComponent<RectTransform>();
		intUI = ui.GetComponent<InteractUI>();
	}
	
	// Update is called once per frame
	void Update () {
		if(show)
		{
			Canvas c = GameObject.Find("UI").GetComponent<Canvas>();

			float sf = (c.scaleFactor);


			Vector3 uiPos = Camera.main.WorldToScreenPoint(new Vector3(transform.position.x, transform.position.y, transform.position.z));

			//print (Screen.width);
			uiPos.x -= Screen.width / 2;
			uiPos.y -= Screen.height / 2;
			uiPos.y += ui.sizeDelta.y;

			uiPos = uiPos / sf;


			ui.anchoredPosition = uiPos;
		}
		show = false;
	}
	public void use()
	{
		if(triggerObject)
		{
			triggerObject.SendMessage("interactUse");
		}
	}
	public void showUI()
	{
		show = true;
		if(intUI)
		{
			intUI.setText(useText);
		}


	}
	public void hideUI()
	{
		show = false;
	}

}
