﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HackingGame : MonoBehaviour {

	private List<Hexagon> hexagons = new List<Hexagon>();

	private EventSystem es;
	private Text timeLeft;
	public float countDown;
	private bool active;
	private Text failText;
	private Text successText;
	private Player player;
	public GameObject hackedObject;
	private Camera hackCam;
	private bool win;

	// Use this for initialization
	void Start () {
		hexagons.AddRange(GetComponentsInChildren<Hexagon>());

		es = GameObject.Find("EventSystem").GetComponent<EventSystem>();
		timeLeft = transform.Find("timeLeftNumbers").GetComponent<Text>();
		timeLeft.text = "" + countDown;

		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		hackCam = GameObject.Find ("Minigame Camera").camera;

		failText = transform.Find ("failText").GetComponent<Text>();
		failText.enabled = false;
		successText = transform.Find ("successText").GetComponent<Text>();
		successText.enabled = false;


	}
	
	// Update is called once per frame
	public void interactUse()
	{
		if(!win)
		{
			setInteractable(true);
			hackCam.depth = 1;
			active = true;
			Screen.lockCursor = false;
			player.hacking = true;
		}

	}
	

	void Update () 
	{

		if(active)
		{
			countDown -= Time.deltaTime;
			if(countDown < 0)
			{
				active = false;
				failText.enabled = true;
				Invoke("endGame", 3f);
			}
			timeLeft.text = countDown.ToString("F1");
		}
	}
	public void endGame()
	{
		setInteractable(false);
		failText.enabled = false;
		countDown = 15;
		foreach(Hexagon h in hexagons)
		{
			h.reset();
		}
		Screen.lockCursor = true;
		player.hacking = false;
		hackCam.depth = -10;
	}
	public void checkSolution()
	{
		es.SetSelectedGameObject(null);
		int c = 0;
		foreach(Hexagon h in hexagons)
		{
			if(h.checkRotation())
			{
				c++;
			}
		}
		if(c == hexagons.Count)
		{
			successText.enabled = true;
			active = false;
			win = true;
			Invoke("endGame", 3f);
			callHackedObject();
		}
	}
	private void setInteractable(bool value)
	{
		foreach(Hexagon h in hexagons)
		{
			h.setActive(value);
		}
	}
	private void callHackedObject()
	{
		hackedObject.SendMessage("hacked");
	}
}
