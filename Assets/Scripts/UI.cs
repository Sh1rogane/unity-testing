﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UI : MonoBehaviour 
{
	public static UI instance;

	private bool showing;

	public Camera sc;

	private RawImage cameraImage;

	private Animator ani;
	private Animator minimapAni;

	private bool minimapMax;
	
	private Camera sensorCamera;

	private Player player;

	private Text motionSensorText;
	private Text cameraText;
	private Text noiseMakerText;
	private Text noText;

	private Slider lightSlider;

	private InteractUI interactUI;

	private int currentCameraIndex = -1;

	// Use this for initialization
	void Start () 
	{
		instance = this;

		cameraImage = this.GetComponentInChildren<RawImage>();

		ani = GetComponentInChildren<Animator>();
		//mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
		sensorCamera = GameObject.Find("Sensor Camera").GetComponent<Camera>();
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

		motionSensorText = transform.Find("MainPanel/ItemPanel/MotionSensorText").gameObject.GetComponent<Text>();
		cameraText = transform.Find("MainPanel/ItemPanel/CameraText").gameObject.GetComponent<Text>();
		noiseMakerText = transform.Find("MainPanel/ItemPanel/NoiseMakerText").gameObject.GetComponent<Text>();

		noText = transform.Find("MainPanel/CameraPanel/NoSignal").GetComponent<Text>();

		lightSlider = transform.Find("MainPanel/LightPanel/Slider").gameObject.GetComponent<Slider>();

		interactUI = transform.Find("InteractUI").GetComponent<InteractUI>();

		minimapAni = GameObject.Find("MinimapCamera").GetComponent<Animator>();
	}
	public void setInteractUI(bool value)
	{
		interactUI.setVisible(value);
	}
	public void setLightSlider(float value)
	{
		lightSlider.value = value;
	}
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Tab))
		{
			showing = !showing;
			ani.SetBool("open", showing);
		}
		if(showing && ani.GetCurrentAnimatorStateInfo(0).IsName("New Animation") && ani.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
		{
			sensorCamera.enabled = true;
		}
		else if(!showing)
		{
			sensorCamera.enabled = false;
		}

		if(Input.GetKeyDown(KeyCode.K))
		{
			setTexture(currentCameraIndex - 1);
		}
		else if(Input.GetKeyDown(KeyCode.L))
		{
			setTexture(currentCameraIndex + 1);
		}

		if(Input.GetKeyDown(KeyCode.M))
		{
			minimapMax = !minimapMax;

			ani.SetBool("maximized", minimapMax);
			minimapAni.SetBool("maximized", minimapMax);
		}
	}
	void FixedUpdate()
	{
		motionSensorText.color = Color.black;
		cameraText.color = Color.black;
		noiseMakerText.color = Color.black;

		if(player.getGadgetIndex() == Player.MOTIONSENSOR){
			motionSensorText.color = Color.red;
		} else if(player.getGadgetIndex() == Player.GADGETCAMERA){
			cameraText.color = Color.red;
		} else {
			noiseMakerText.color = Color.red;
		}
		motionSensorText.text = "M " + player.motionSensor;
		cameraText.text = "C " + player.gadgetCamera;
		noiseMakerText.text = "N " + player.noiseMaker;
	}
	public void setTexture(int pos)
	{
		if(currentCameraIndex >= 0 && currentCameraIndex < GadgetCamera.cameras.Count)
			GadgetCamera.cameras[currentCameraIndex].unselect();
	

		currentCameraIndex = pos;

		if(currentCameraIndex > GadgetCamera.cameras.Count - 1)
		{
			currentCameraIndex = 0;

		} else if(currentCameraIndex < 0)
		{
			currentCameraIndex = GadgetCamera.cameras.Count - 1;
		}


		if(GadgetCamera.cameras.Count == 0)
		{
			cameraImage.texture = null;
			noText.enabled = true;
		}
		else
		{
			noText.enabled = false;
			cameraImage.texture = GadgetCamera.cameras[currentCameraIndex].renderTexture;
			GadgetCamera.cameras[currentCameraIndex].select();
		}

	}
}
