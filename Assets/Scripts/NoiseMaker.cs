﻿using UnityEngine;
using System.Collections;

public class NoiseMaker : MonoBehaviour 
{
	private float soundTimer;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{

		soundTimer += Time.deltaTime;
		if(soundTimer >= 5f)
		{
			soundTimer = 0f;
			SoundManager.addSound(this.gameObject, transform.position, 20f);
			Destroy(this.gameObject);
		}
	}
}
