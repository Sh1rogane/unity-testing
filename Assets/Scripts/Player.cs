using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
	public static Object prefab = Resources.Load("GadgetCamera");
	public static Object noiseMakerPrefab = Resources.Load("NoiseMaker");
	public static Object placeCameraPrefab = Resources.Load("PlaceCamera");
	public static Object motionSensorPrefab = Resources.Load ("MotionSensor");

	private CharacterController cc;

	public float sensitivity = 1;
	public float speed = 3; 

	private float runSpeed;
	private float crouchSpeed;

	private bool running;
	private bool croching;
	private Vector3 crouchVector = new Vector3(0,0.8f,0f);
	private Vector3 normalVector = new Vector3(0,1.6f,0f);

	private bool leanLeft;
	private bool leanRight;

	public Camera c;
	private Camera sc;

	private float rotationX;
	private float rotationY;
	
	public float minimumY = -60;
	public float maximumY = 60;

	public float jumpSpeed = 8.0F;
	public float gravity = 20.0F;
	private Vector3 moveDirection = Vector3.zero;

	private Transform leanHelper;
	private Vector3 leanAngle = Vector3.zero;

	private Transform throwHelper;

	private UI ui;

	private GameObject placeCameraObject;

	private float moveTimer = 0f;

	private bool alive = true;

	//Inventory
	public int motionSensor = 2;
	public int gadgetCamera = 3;
	public int noiseMaker = 5;
	public static int MOTIONSENSOR = 0;
	public static int GADGETCAMERA = 1;
	public static int NOISEMAKER = 2;
	private int gadgetIndex = 0;

	private Animator armAni;
	private bool isHacking;
	public bool hacking
	{
		get{return isHacking;}
		set{
			ui.setInteractUI(false);
			isHacking = value;
		}
	}

	private float visibility;

	private float dashSpeed;
	private bool dashing;
	private float mouseSensitivity = 1;

	// Use this for initialization
	void Start () 
	{

		this.cc = this.GetComponent<CharacterController> ();
		c = transform.Find("Lean Helper/Main Camera").gameObject.GetComponent<Camera>();
		sc = transform.Find("Lean Helper/Sensor Camera").gameObject.GetComponent<Camera>();
		leanHelper = transform.Find("Lean Helper").gameObject.GetComponent<Transform>();
		throwHelper = transform.Find("Lean Helper/Main Camera/ThrowHelper").gameObject.GetComponent<Transform>();
		ui = GameObject.Find("UI").GetComponent<UI>();

		armAni = GameObject.Find ("ArmCamera").GetComponentInChildren<Animator>();

		Screen.lockCursor = true;

		runSpeed = speed + 2;
		crouchSpeed = speed - 1;

		//Physics.IgnoreCollision(c.collider, cc);

	}
	void FixedUpdate()
	{
		checkLight();
	}
	void checkLight()
	{
		float lightPower = 0;

		GameObject[] gos = GameObject.FindGameObjectsWithTag("Light");
		foreach(GameObject go in gos)
		{
			Light l = go.GetComponent<Light>();

			float dist = Vector3.Distance(transform.position, l.transform.position);
			float range = l.range;
			float visi = range - dist;

			if(visi > 0)
			{
				lightPower += visi;
			}
		}
		visibility = lightPower;
		visibility = Mathf.Clamp(visibility, 0, 5);
		//print (lightPower);
		ui.setLightSlider(lightPower);
	}
	public float getVisibility()
	{
		return visibility;
	}
	public void kill()
	{
		alive = false;
		print ("HAHA you died");

	}
	//Not in use
	void UpdateTrajectory(Vector3 initialPosition, Vector3 initialVelocity, Vector3 gravity)
	{
		int numSteps = 20; // for example
		float timeDelta = 1.0f / initialVelocity.magnitude; // for example
		
		LineRenderer lineRenderer = GetComponentInChildren<LineRenderer>();
		lineRenderer.SetVertexCount(numSteps);
		
		Vector3 position = initialPosition;
		Vector3 velocity = initialVelocity;
		for (int i = 0; i < numSteps; ++i)
		{
			lineRenderer.SetPosition(i, position);
			
			position += velocity * timeDelta + 0.5f * gravity * timeDelta * timeDelta;
			velocity += gravity * timeDelta;
		}
	}

	// Update is called once per feeeeerame
	void Update () 
	{
		if(!isHacking && alive)
		{
			//float horizontal = Input.GetAxis("Horizontal");
			float vertical = Input.GetAxis("Vertical");

			float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
			float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

			//If pressed c change to crouch or standing
			if(Input.GetKeyDown(KeyCode.C))
			{
				croching = !croching;
				if(croching)
				{
					cc.height = 1;
					cc.center = new Vector3(0, 0.5f,0);
				}
				else
				{
					cc.height = 1.8f;
					cc.center = new Vector3(0, 0.9f,0);
				}
			}
			//If the player is crouching, lerp the camera to the crouch position else same but standing
			if(croching)
			{
				c.transform.localPosition = Vector3.Lerp(c.transform.localPosition, crouchVector, Time.deltaTime * 5);
			}
			else
			{
				c.transform.localPosition = Vector3.Lerp(c.transform.localPosition, normalVector, Time.deltaTime * 5);
			}

			//Running key and only forwards and not sroching
			if(Input.GetKey(KeyCode.LeftShift) && vertical > 0 && !croching)
			{
				running = true;
			}
			else
			{
				running = false;
			}

			//Leaning
			if(Input.GetKey(KeyCode.E))
			{
				leanRight = true;
			}
			else
			{
				leanRight = false;
			}
			if(Input.GetKey(KeyCode.Q) && !leanRight)
			{
				leanLeft = true;
			}
			else
			{
				leanLeft = false;
			}
			//Lerp the lean
			if(leanRight)
			{
				leanAngle = Vector3.Lerp(leanAngle, new Vector3(0,0,-20), Time.deltaTime * 3);
			}
			else if(leanLeft)
			{
				leanAngle = Vector3.Lerp(leanAngle, new Vector3(0,0,20), Time.deltaTime * 3);
			}
			else
			{
				leanAngle = Vector3.Lerp(leanAngle, new Vector3(0,0,0), Time.deltaTime * 5);
			}
			leanHelper.localRotation = Quaternion.Euler(leanAngle);

			rotationX += mouseX;
			rotationY += mouseY;

			Adjust360andClamp ();

			//If player is on ground, the player can move and jump and run
			if (cc.isGrounded) 
			{
				armAni.speed = 1;
				//Gets the direction from input
				moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
				moveDirection = transform.TransformDirection(moveDirection);
				//Clamp the movement so you can't strafe run faster
				moveDirection = Vector3.ClampMagnitude (moveDirection, 1f);
				//If the player is running, make sound each 0.3 second
				if(running)
				{
					armAni.speed = 2;
					moveTimer += Time.deltaTime;
					if(moveTimer > 0.3f)
					{
						moveTimer = 0f;
						SoundManager.addSound(gameObject, transform.position, 10f);
					}
					moveDirection *= runSpeed;
				}
				else if(croching)
				{
					moveDirection *= crouchSpeed;
				}
				else
				{
					moveDirection *= speed;
				}

				//if (Input.GetButtonDown("Jump"))
				//	moveDirection.y = jumpSpeed;
				
			}
			if(Input.GetButtonDown("Jump") && !dashing)
			{
				dashing = true;
				dashSpeed = 5;
				mouseSensitivity = 0.3f;
			}
			if(dashing)
			{
				moveDirection = transform.forward * dashSpeed;
				dashSpeed -= Time.deltaTime * 8;
				if(dashSpeed <= 0)
				{
					mouseSensitivity = 1f;
					dashing = false;
				}
			}
			//Update the charactercontroller and move it
			moveDirection.y -= gravity * Time.deltaTime;
			cc.Move (moveDirection * speed * Time.deltaTime);

			armAni.SetFloat("speed", Mathf.Abs(moveDirection.x) + Mathf.Abs(moveDirection.z));

			transform.rotation = Quaternion.AngleAxis (rotationX, Vector3.up);
			c.transform.localRotation = Quaternion.AngleAxis (rotationY, Vector3.left);

			sc.transform.localRotation = c.transform.localRotation;
			sc.transform.localPosition = c.transform.localPosition;

			//Selects the gadget
			gadgetSelecter();


			if(gadgetIndex == GADGETCAMERA){
				placeCamera();
			}else{
				if(placeCameraObject != null)
				{
					Destroy(placeCameraObject);
				}
			}

			if(Input.GetButtonDown("Fire1")){
				//Lock the cursor again
				Screen.lockCursor = true;
				if(gadgetIndex == MOTIONSENSOR){
					throwMotionDetector();
				} else if(gadgetIndex == NOISEMAKER){
					throwNoiseMaker();
				}

			}

			checkPickup();

			//UpdateTrajectory(throwHelper.position, c.transform.forward * 15, Physics.gravity);
		}
	}
	public int getGadgetIndex(){
		return gadgetIndex;
	}
	//Uses a raycast forward and checks if it is any Interactable objects in the way and if it is. show the UI
	void checkPickup()
	{
		RaycastHit hit;
		LayerMask lm = 1 << 13;

		if(Physics.Raycast(c.transform.position, c.transform.forward, out hit, 3f, lm))
		{
			if(hit.collider.gameObject.GetComponent<Interactable>())
			{
				//Show pick up UI
				ui.setInteractUI(true);
				hit.collider.gameObject.GetComponent<Interactable>().showUI();
				//And then should check for input calling some function in Interactable
				if(Input.GetKeyDown(KeyCode.F))
				{
					armAni.SetTrigger("pickup");
					hit.collider.gameObject.GetComponent<Interactable>().use();
				}
			}
		}
		else
		{
			ui.setInteractUI(false);
		}
	}
	void throwMotionDetector()
	{
		//needs to show on GUI later (Does not know how to make things attached to the GUI)
		if (motionSensor != 0) {
			//Play animation and wait 0.3 seconds before creating object
			armAni.SetTrigger("throw");
			Invoke("createMotionSensor", 0.3f);
			motionSensor--;
		} else {
			Debug.Log ("Out of motion sensors");
		}
	}
	void createMotionSensor()
	{
		GameObject newSensor = Instantiate (motionSensorPrefab) as GameObject;
		newSensor.transform.position = throwHelper.position;
		newSensor.rigidbody.velocity = c.transform.forward * 15;
	}
	void gadgetSelecter(){
		
		// scroll up
		if(Input.GetAxis("Mouse ScrollWheel") > 0){
			gadgetIndex++;

			if(gadgetIndex > 2)
			{
				gadgetIndex = 0;
			}
		}
		//scroll down
		else if(Input.GetAxis("Mouse ScrollWheel") < 0){
			gadgetIndex--;
			if(gadgetIndex < 0)
			{
				gadgetIndex = 2;
			}
		}
		//Change with 1,2,3 etc
		if(Input.GetKeyDown(KeyCode.Alpha1))
		{
			gadgetIndex = 0;
		} else if(Input.GetKeyDown(KeyCode.Alpha2))
		{
			gadgetIndex = 1;
		} else if(Input.GetKeyDown(KeyCode.Alpha3))
		{
			gadgetIndex = 2;
		}

	}
	void throwNoiseMaker()
	{
		if (noiseMaker != 0) {
			//Play animation and wait 0.3 seconds before creating object
			armAni.SetTrigger("throw");
			Invoke("createNoiseMaker", 0.3f);
			noiseMaker--;
		} else {
			Debug.Log ("Out of noise makers");
		}
	}
	void createNoiseMaker()
	{
		GameObject newObject = Instantiate (noiseMakerPrefab) as GameObject;
		newObject.transform.position = throwHelper.position;
		newObject.rigidbody.velocity = c.transform.forward * 15;
	}
	void placeCamera()
	{
		RaycastHit hit;

		LayerMask lm = 1 << 10;
		lm |= 1 << 2; 
		lm = ~lm;

		//Do raycast forward
		if (Physics.Raycast (c.transform.position, c.transform.forward, out hit, 5f, lm))
		{
			//If it hits and distance
			if(hit.distance < 5f)
			{
				//If preview is not created, create it
				if(placeCameraObject == null)
				{
					placeCameraObject = Instantiate(placeCameraPrefab, hit.point, Quaternion.LookRotation(-c.transform.forward)) as GameObject;
				}
				else
				{
					//Else reposition it
					placeCameraObject.transform.position = hit.point;
					placeCameraObject.transform.rotation = Quaternion.LookRotation(hit.normal);
				}
				//If the gadget is the camera and player pressed fire1 create the camera
				if(gadgetIndex == GADGETCAMERA && Input.GetButtonDown("Fire1")){
					createCamera(hit);
				}
			}
			//If not hitting anything remove the preview camera if created
			else
			{
				if(placeCameraObject != null)
				{
					Destroy(placeCameraObject);
				}
			}
		}
		else
		{
			if(placeCameraObject != null)
			{
				Destroy(placeCameraObject);
			}
		}

	}
	//Creates the gadget camera and sets the UI render texture to that camera
	void createCamera(RaycastHit hit)
	{
		//If has gadgets cameras
		if (gadgetCamera != 0) {

			//Play "place" animation, not in use
			armAni.SetTrigger("pickup");
			//Instantiate the camera prefab
			GameObject newObject = Instantiate (prefab) as GameObject;
			Camera instance = newObject.GetComponent<Camera> ();
			//Set the position and rotation according to ray
			instance.transform.position = hit.point;
			instance.transform.localRotation = Quaternion.LookRotation (hit.normal);

			gadgetCamera--;
		} else {
			Debug.Log ("you are out of cameras");
		}
	}
	//Used to keep rotation between 0-360
	void Adjust360andClamp ()
	{
		if (rotationX < -360)
		{
			rotationX += 360;
		}
		else if (rotationX > 360)
		{
			rotationX -= 360;
		}   
		if (rotationY < -360)
		{
			rotationY += 360;
		}
		else if (rotationY > 360)
		{
			rotationY -= 360;
		}
		
		// Clamp our angles to the min and max set in the Inspector
		//rotationX = Mathf.Clamp (rotationX, minimumX, maximumX);
		rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
	}
	public Vector3 getEyePosition()
	{
		Vector3 pos = transform.position;
		if(croching)
		{
			pos.y += 0.9f;
		}
		else
		{
			pos.y += 1.7f;
		}

		return pos;
	}
	public Vector3 getRaycastPosition()
	{
		Vector3 pos = transform.position;
		pos.y += cc.center.y;
		return pos;
	}
}
