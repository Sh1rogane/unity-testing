﻿using UnityEngine;
using System.Collections;

public class GameLight : MonoBehaviour 
{
	public bool on;

	private float startRange;
	// Use this for initialization
	void Start () {
		startRange = light.range;
		if(light.range > 0)
		{
			on = true;
		}
		else
		{
			on = false;
		}
		//changeState(false);
	}
	void FixedUpdate()
	{

	}
	// Update is called once per frame
	void Update () {
	}
	//Changes the state of the light
	public void changeState()
	{
		on = !on;
		if(on)
		{
			light.range = startRange;
		}
		else
		{
			light.range = 0;
		}
		raycastToAllEnemies();
	}
	public void interactUse()
	{
		changeState();
	}
	private void raycastToAllEnemies()
	{
		//Find all enemies in scene
		GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");

		foreach(GameObject go in gos)
		{
			Vector3 targetDir = (go.transform.position) - transform.position;
			RaycastHit hit;
			//Raycast to enemy
			if (Physics.Raycast(transform.position, targetDir, out hit, 30f)) 
			{
				//If raycast hits enemy
				if(hit.collider.gameObject.tag.Equals("Enemy"))
				{

					Debug.DrawLine(transform.position, hit.point, Color.red, 1f);
					//Notify enemy that a change in light happend
				}
				Debug.DrawLine(transform.position, hit.point, Color.green, 1f);
			}
		}
	}
}
