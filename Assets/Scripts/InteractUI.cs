﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InteractUI : MonoBehaviour {

	private Image panel;
	private Text useText;
	private Image panel2;
	private Text interactKey;

	// Use this for initialization
	void Start () {
		panel = GetComponent<Image>();
		useText = transform.Find("UseText").gameObject.GetComponent<Text>();
		panel2 = transform.Find("Panel").gameObject.GetComponent<Image>();
		interactKey = transform.Find("Panel/InteractKey").gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setVisible(bool value)
	{
		panel.enabled = value;
		useText.enabled = value;
		panel2.enabled = value;
		interactKey.enabled = value;
	}
	public void setText(string s)
	{
		useText.text = s;
	}
}
