﻿using UnityEngine;
using System.Collections;

public class LightSwitch : MonoBehaviour 
{

	public GameLight[] lights;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void interactUse()
	{
		foreach(GameLight gl in lights)
		{
			gl.changeState();
		}
	}
}
