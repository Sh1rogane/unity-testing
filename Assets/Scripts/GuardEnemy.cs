﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GuardEnemy : MonoBehaviour 
{
	//States
	private enum States {Idle, Patrolling, Investigate, Chasing, Search};
	private States currentState = States.Idle;

	//Component stuff
	private Animator animator;
	private NavMeshAgent navAgent;
	private AudioSource stepAudio;

	//NavmeshAgent
	private float walkSpeed = 2f;
	private float chaseSpeed = 4f;

	//Start stuff
	private Vector3 startPos;
	private Quaternion startRot;

	private Transform head;
	private Transform headJoint;

	//Field of view stuff
	private float fieldOfView = 140f;
	private float viewDistance = 20f;

	private Player player;
	private Vector3 lookAt;

	//Detected meter stuff
	//0 - Nothing
	//100 - Know
	private float detectedMeter = 0;
	private bool detectedIncrease;
	private float detectedITimer = 0;

	//Knowledge stuff
	private Vector3 lastSeenPos = Vector3.zero;
	private Vector3 investigatePos = Vector3.zero;

	//Investigate
	private float stayTimer = 0f;

	//Patrol stuff
	public List<PatrolNode> patrolNodes = new List<PatrolNode>();
	private int currentNodeIndex = 0;
	//private float pauseTime;

	//Chase
	private float chaseTimer = 0f;
	private float chaseTime = 3f;
	private Vector3 chasePosition = Vector3.zero;

	//Search
	private List<SearchNode> searchNodes = new List<SearchNode>(); 
	private int currentSearchNode = 0;

	//Testing navmesh stuff
	public Transform target;

	// Use this for initialization
	void Start () 
	{
		animator = GetComponent<Animator>();
		navAgent = GetComponent<NavMeshAgent>();
		stepAudio = GetComponentInChildren<AudioSource>();

		head = transform.Find("Basic_Guard/Root/Spine_01/Neck/Headjoint/Headtip");
		headJoint = transform.Find("Basic_Guard/Root/Spine_01/Neck/Headjoint");

		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

		lookAt = head.position + head.forward * 10f;

		startPos = transform.position;
		startRot = transform.rotation;

		if(patrolNodes.Count > 0)
		{
			currentState = States.Patrolling;
			navAgent.SetDestination(patrolNodes[currentNodeIndex].transform.position);
		}
	}
	void FixedUpdate()
	{
		seeing();
		if(currentState == States.Idle)
		{
			idle();
		}
		else if(currentState == States.Patrolling)
		{
			patroll();
		}
		else if(currentState == States.Investigate)
		{
			investigate();
		}
		else if(currentState == States.Chasing)
		{
			chasing();
		}
		else if(currentState == States.Search)
		{
			search();
		}

		float dist = Vector3.Distance(getEyePosition(), player.getEyePosition());
		if(dist < 1f)
		{
			navAgent.Stop ();
			animator.SetTrigger("attack");
			player.kill();
		}
	}
	private void gatherSearchNodes(Vector3 center, float radius)
	{
		searchNodes.Clear();

		Collider[] hitColliders = Physics.OverlapSphere(center, radius, 1 << 12);

		foreach(Collider c in hitColliders)
		{
			if(c.tag.Equals("SearchNode"))
			{
				SearchNode sn = c.GetComponent<SearchNode>();
				searchNodes.Add(sn);
			}
		}

		print (searchNodes.Count);
	}
	private void search()
	{
		if(detectedMeter >= 50 && detectedMeter < 100)
		{
			lastSeenPos = player.transform.position;
			currentState = States.Investigate;
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}
		else if(detectedMeter >= 100)
		{
			lastSeenPos = player.transform.position;
			currentState = States.Chasing;
		}

		if(navAgentDone())
		{
			searchNodes.RemoveAt(0);
		}
		if(searchNodes.Count > 0)
		{
			navAgent.SetDestination(searchNodes[0].transform.position);
		}
		else
		{
			if(patrolNodes.Count > 0)
			{
				currentState = States.Patrolling;
				navAgent.SetDestination(patrolNodes[currentNodeIndex].transform.position);
				chasePosition = Vector3.zero;
				investigatePos = Vector3.zero;
			}
			else
			{
				currentState = States.Idle;
				navAgent.SetDestination(startPos);
				chasePosition = Vector3.zero;
				investigatePos = Vector3.zero;
			}
		}
		
	}
	private void chasing()
	{
		navAgent.speed = chaseSpeed;

		if(navAgentDone())
		{
			chaseTimer += Time.fixedDeltaTime;
			if(chaseTimer >= chaseTime)
			{
				chaseTimer = 0f;
				detectedMeter = 30;
				navAgent.speed = walkSpeed;
				gatherSearchNodes(transform.position, 20f);
				navAgent.SetDestination(searchNodes[0].transform.position);
				currentState = States.Search;
			}
		}


	}
	private void investigate()
	{
		//Walk to last seen "suspected" position;
		if(detectedMeter >= 50 && detectedMeter < 100)
		{
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}
		else if(detectedMeter >= 100)
		{
			lastSeenPos = player.transform.position;
			currentState = States.Chasing;
		}

		if(navAgentDone())
		{
			stayTimer += Time.fixedDeltaTime;
			if(stayTimer > 3f)
			{
				investigatePos = Vector3.zero;
				stayTimer = 0f;
				if(patrolNodes.Count > 0)
				{
					currentState = States.Patrolling;
					navAgent.SetDestination(patrolNodes[currentNodeIndex].transform.position);
				}
				else
				{
					currentState = States.Idle;
					navAgent.SetDestination(startPos);
				}
			}


		}
	}
	private void patroll()
	{
		//Enemy thinks he sees something
		if(detectedMeter > 50 && detectedMeter < 100)
		{
			lastSeenPos = player.transform.position;
			currentState = States.Investigate;
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}
		//Enemy sees the player
		else if(detectedMeter >= 100)
		{
			lastSeenPos = player.transform.position;
			currentState = States.Chasing;
		}
	}
	private void idle()
	{
		if(navAgentDone())
		{
			transform.rotation = Quaternion.RotateTowards(transform.rotation, startRot, Time.fixedDeltaTime * 90);
		}
		//Enemy thinks he sees something
		if(detectedMeter > 50 && detectedMeter < 100)
		{
			lastSeenPos = player.transform.position;
			currentState = States.Investigate;
			navAgent.SetDestination(lastSeenPos);
			investigatePos = lastSeenPos;
		}
		//Enemy sees the player
		else if(detectedMeter >= 100)
		{
			lastSeenPos = player.transform.position;
			currentState = States.Chasing;
		}
	}
	// Update is called once per frame
	void Update () 
	{

		//navAgent.SetDestination(target.position);
		animator.SetFloat("speed", navAgent.desiredVelocity.magnitude);
	}


	void OnTriggerEnter(Collider col)
	{
		if(currentState == States.Patrolling)
		{
			if(col.gameObject.tag.Equals("Node"))
			{
				PatrolNode n = col.gameObject.GetComponent<PatrolNode>();
				if(patrolNodes[currentNodeIndex] == n)
				{
					currentNodeIndex++;
					if(currentNodeIndex >= patrolNodes.Count)
					{
						currentNodeIndex = 0;
					}
					Invoke("changePatrolNode", n.pauseTime); 
				}
			}
		}

	}
	void changePatrolNode()
	{
		navAgent.SetDestination(patrolNodes[currentNodeIndex].transform.position);
	}
	bool navAgentDone()
	{
		//if (!navAgent.pathPending)
		//{
		if (navAgent.remainingDistance <= navAgent.stoppingDistance)
		{
			if (!navAgent.hasPath || navAgent.velocity.sqrMagnitude == 0f)
			{
				return true;
			}
		}
		//}
		return false;
	}
	void seeing()
	{
		//First check if player is inside the FOV
		if(playerInsideFOV())
		{
			//If player is inside FOV do a raycast to the player with a small random factor
			if(raycastToPlayerRandom())
			{
				if(currentState == States.Chasing)
				{
					chasePosition = player.transform.position;
					navAgent.SetDestination(chasePosition);
				}

				//If player is hit, calculate the visibility
				float dm = checkPlayerVisibility();
				//If visibility is higher than 0 (the enemy can see atleast something)
				if(dm > 0)
				{
					//Add the calulated value to detected meter
					detectedITimer = 0f;
					detectedIncrease = true;
					detectedMeter += dm;
				}
			}
		}
		//This is used to indicate when it is time to lower detected meter (if enemy can't see anything in 1 second)
		if(detectedIncrease)
		{
			detectedITimer += Time.fixedDeltaTime;
			if(detectedITimer > 1f)
			{
				detectedIncrease = false;
			}
		}
		else
		{

			detectedMeter -= 1;
		}
		//Clamp detected meter
		detectedMeter = Mathf.Clamp(detectedMeter, 0, 200);
		print (detectedMeter);

	}
	float checkPlayerVisibility()
	{
		float dist = Vector3.Distance(getEyePosition(), player.getRaycastPosition());
		float playerVisibility = player.getVisibility();
		float relativeDist = (viewDistance - dist) / 5; 
		if(relativeDist < 1)
		{
			relativeDist = 1;
		}
		//print (dist);
		float dm = relativeDist * (playerVisibility / 2);


		//dist = 20 , vis = 5;
		//relDist 2 * 5 10 1/5 sekund

		if(dist <= 2f && playerVisibility < 1)
		{
			return 3;
		}

		//detectedMeter += dm;
		return dm;
	}
	bool raycastToPlayer()
	{
		Vector3 targetDir = (player.getRaycastPosition()) - getEyePosition();
		RaycastHit hit;
		
		if (Physics.Raycast(getEyePosition(), targetDir, out hit, viewDistance)) 
		{
			if(hit.collider.gameObject.tag.Equals("Player"))
			{
				Debug.DrawLine(getEyePosition(), hit.point, Color.red, 0.1f);
				return true;
			}
			Debug.DrawLine(getEyePosition(), hit.point, Color.green, 0.1f);
		}
		return false;
	}
	bool raycastToPlayerRandom()
	{
		Vector3 targetDir = (getRandomPlayerPosition()) - getEyePosition();
		RaycastHit hit;
		
		if (Physics.Raycast(getEyePosition(), targetDir, out hit, Mathf.Infinity)) 
		{
			
			if(hit.collider.gameObject.tag.Equals("Player"))
			{
				Debug.DrawLine(getEyePosition(), hit.point, Color.red, 0.1f);
				return true;
			}
			Debug.DrawLine(getEyePosition(), hit.point, Color.green, 0.1f);
		}
		return false;
	}
	bool playerInsideFOV()
	{		
		Vector3 targetDir = player.transform.position - getEyePosition();
		float a = Vector3.Angle(targetDir, head.forward);
		float dist = Vector3.Distance(getEyePosition(), player.getRaycastPosition());
		//print (a);
		if((a < fieldOfView / 2))
		{
			return true;
		}
		return false;
	}
	private Vector3 getRandomPlayerPosition()
	{
		Vector3 pos = player.getRaycastPosition();
		pos += new Vector3(Random.Range(-0.5f,0.5f), Random.Range(-0.5f,0.5f), Random.Range(-0.5f,0.5f));
		return pos;
	}
	private Vector3 getEyePosition()
	{
		Vector3 pos = head.position;
		pos.y -= 0.1f;

		return pos;
	}
	public void StepSound()
	{
		stepAudio.Play ();
	}
	void OnAnimatorIK(int layerIndex)
	{
		animator.SetLookAtPosition(lookAt);
		animator.SetLookAtWeight(1f);
		
		if(detectedMeter > 50)
		{
			lookAt = Vector3.Lerp(lookAt, player.getEyePosition(), Time.deltaTime * 3);
		}
		else
		{
			lookAt = Vector3.Lerp(lookAt, transform.position + transform.forward * 10f, Time.deltaTime * 3);
		}
		
	}
	public Vector3 getRaycastPosition()
	{
		Vector3 pos = transform.position;
		pos.y += 0.9f;
		return pos;
	}
	void OnDrawGizmosSelected()
	{
		drawView ();
		drawPatrolPath();
	}
	private void drawView()
	{
		Gizmos.color = Color.magenta;
		if(head != null)
		{
			Vector3 start = head.position;
			start.y -= 0.1f;
			
			float halfAngle = fieldOfView / 2f;
			
			float ca = -halfAngle;
			float l = fieldOfView / 10f;
			float la = fieldOfView / l;
			for(int i = 0; i < l; i++)
			{
				Quaternion q1 = Quaternion.Euler (head.rotation.eulerAngles.x, head.rotation.eulerAngles.y + ca, head.rotation.eulerAngles.z);
				Vector3 forward = q1 * Vector3.forward;
				forward.Normalize ();
				RaycastHit hit;
				if (Physics.Raycast (start, forward, out hit, viewDistance)) 
				{
					Gizmos.DrawLine (start, hit.point);
				} 
				else 
				{
					Gizmos.DrawLine (start, start + forward * viewDistance);
				}
				ca += la;
			}
		}
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere(lookAt, 0.3f);

		if(investigatePos != Vector3.zero)
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(investigatePos, 1f);
		}

		if(chasePosition != Vector3.zero)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(chasePosition, 1f);

			//Gizmos.DrawWireSphere(chasePosition, 20f);
		}
	}
	private void drawPatrolPath()
	{
		Gizmos.color = Color.blue;
		if(searchNodes.Count > 0)
		{
			for(int i = 0; i < searchNodes.Count - 1; i++)
			{
				Gizmos.DrawLine(searchNodes[i].transform.position, searchNodes[i + 1].transform.position);
			}
			if(searchNodes.Count > 1)
			{
				//Gizmos.DrawLine(searchNodes[searchNodes.Count - 1].transform.position, searchNodes[0].transform.position);
			}
		}
		
		
	}
}
