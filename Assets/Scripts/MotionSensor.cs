﻿using UnityEngine;
using System.Collections;

public class MotionSensor : MonoBehaviour {


	private GuardEnemy enemy;
	private GameObject blip;
	private Vector3 lastPos;
	private bool enemyInside;
	private bool enemyInSight;
	private bool enemyInSightFrame;
	private bool enemyLostFrame;
	private float motionTimer = 0f;
	private float exitTimer = 0f;
	public int viewDistance = 20;
	private SphereCollider sphere;

	// Use this for initialization
	void Start () {
		sphere = this.GetComponent<SphereCollider> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(enemyInside && enemy != null)
		{
			enemyInSight = false;
			if(sightCheck(enemy.getRaycastPosition()))
			{
				enemyInSight = true;
				enemyLostFrame = false;
				if(!enemyInSightFrame)
				{
					enemyInSightFrame = true;
					gotSignal();
				}
			}

			if(enemyInSight)
			{
				motionTimer += Time.deltaTime;
				if(motionTimer > 2f)
				{
					motionTimer = 0;
					blip = MotionBlip.createInstance(enemy.getRaycastPosition());
				}
			}
			else if(!enemyInSight && enemyInSightFrame)
			{
				enemyInSightFrame = false;
				if(!enemyLostFrame)
				{
					enemyLostFrame = true;
					Invoke ("lostSignal", 1f);
				}
			}
			/*if(sightCheck(enemy.transform.position)){
				motionTimer += Time.deltaTime;
				if(motionTimer > 2f)
				{
					motionTimer = 0;
					blip = MotionBlip.createInstance(enemy.transform.position);
				}
			}
			else
			{

			}*/
		}
	}
	public void interactUse()
	{
		Destroy(gameObject);
		GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().motionSensor++;
	}
	bool sightCheck(Vector3 enemyDir)
	{
		Vector3 dir = (enemyDir) - transform.position;

		RaycastHit hit;

		LayerMask lm = 1 << 13;
		lm |= 1 << 2;
		lm = ~lm;

		if (Physics.Raycast (transform.position, dir, out hit, sphere.radius, lm)) {

			//Debug.Log(hit.collider);
			Debug.DrawLine (transform.position, hit.point, Color.red, 0.1f);
			if (hit.collider.gameObject.tag.Equals("Enemy") && (hit.distance < sphere.radius)) {


				lastPos = hit.point;
			
				return true;
			}

		}
		
		return false;
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag.Equals("Enemy"))
		{

			enemyInside = true;
			enemy = col.GetComponent<GuardEnemy>();
			enemyInSightFrame = false;
			//gotSignal();
		} 
	}
	void OnTriggerStay(Collider col)
	{

		/*if(col.gameObject.tag.Equals("Enemy"))
		{	
		
			enemy = col.GetComponent<Enemy>();
			enemyInside = true;

		} */

	}
	void gotSignal()
	{
		motionTimer = 0f;
		blip = MotionBlip.createInstance (enemy.getRaycastPosition());
		blip.GetComponentInChildren<Renderer>().material.color = new Color(255, 71, 0, 0.1f);
		blip.GetComponent<AudioSource>().loop = true;
		blip.GetComponent<AudioSource> ().pitch = 2f;
	}
	void lostSignal()
	{
		blip = MotionBlip.createInstance (enemy.getRaycastPosition());
		blip.GetComponentInChildren<Renderer> ().material.color = new Color (255, 0, 0, 0.3f);
		blip.GetComponent<AudioSource> ().loop = true;
		blip.GetComponent<AudioSource> ().pitch = 0.5f;
	}
	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag.Equals("Enemy") && enemyInSight)
		{
			enemyLostFrame = false;
			Invoke ("lostSignal", 1f);
			enemyInside = false;
			enemy = null;
		}
	}
}
